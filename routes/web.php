<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::resource('alumnos', 'AlumnoController');

Route::resource('catedraticos', 'CatedraticoController');
Route::resource('contratos', 'ContratoController');
Route::resource('grados', 'GradoController');
Route::resource('inscripcions', 'InscripcionController');
Route::resource('notas', 'NotaController');
Route::resource('seccions', 'SeccionController');
Route::resource('sucursals', 'SucursalController');
