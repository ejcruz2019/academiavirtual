<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SucursalPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'id_sucursal' => [
                'required',
            ],
            'nombre' => [
                'required',
            ],
            'razon_social' => [
                'required',
            ],
            'direccion' => [
                'required',
            ],
            'telefono' => [
                'required',
            ],
            'correo' => [
                'required',
            ],
        ];
    }
}
