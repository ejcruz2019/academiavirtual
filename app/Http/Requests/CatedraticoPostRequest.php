<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CatedraticoPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'id_catedratico' => [
                'required',
            ],
            'nombre' => [
                'required',
            ],
            'apellido' => [
                'required',
            ],
            'dpi' => [
                'required',
            ],
            'telefono' => [
                'required',
            ],
            'correo' => [
                'required',
            ],
        ];
    }
}
