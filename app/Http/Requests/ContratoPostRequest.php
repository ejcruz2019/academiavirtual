<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContratoPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'id_contrato' => [
                'required',
            ],
            'seccion_id_seccion' => [
                'required',
            ],
            'catedratico_id_catedratico' => [
                'required',
            ],
            'fecha' => [
                'required',
            ],
        ];
    }
}
