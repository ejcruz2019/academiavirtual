<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SucursalPostRequest;
use App\Http\Models\Sucursal\Sucursal;


class SucursalController extends Controller
{

    public function index()
    {
        $sucursals = Sucursal::all();
        return view('sucursals.index', compact('sucursals'));
    }

    public function show(Request $request, Sucursal $sucursal)
    {
        return view('sucursals.show', compact('sucursal'));
    }

    public function create()
    {
        return view('sucursals.create');
    }

    public function store(SucursalPostRequest $request)
    {
        $data = $request->validated();
        $sucursal = Sucursal::create($data);
        return redirect()->route('sucursals.index')->with('status', 'Sucursal created!');
    }

    public function edit(Request $request, Sucursal $sucursal)
    {
        return view('sucursals.edit', compact('sucursal'));
    }

    public function update(SucursalPostRequest $request, Sucursal $sucursal)
    {
        $data = $request->validated();
        $sucursal->fill($data);
        $sucursal->save();
        return redirect()->route('sucursals.index')->with('status', 'Sucursal updated!');
    }

    public function destroy(Request $request, Sucursal $sucursal)
    {
        $sucursal->delete();
        return redirect()->route('sucursals.index')->with('status', 'Sucursal destroyed!');
    }
}
