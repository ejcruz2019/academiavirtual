<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SeccionPostRequest;
use App\Http\Models\Seccion\Seccion;


class SeccionController extends Controller
{

    public function index()
    {
        $seccions = Seccion::all();
        return view('seccions.index', compact('seccions'));
    }

    public function show(Request $request, Seccion $seccion)
    {
        return view('seccions.show', compact('seccion'));
    }

    public function create()
    {
        return view('seccions.create');
    }

    public function store(SeccionPostRequest $request)
    {
        $data = $request->validated();
        $seccion = Seccion::create($data);
        return redirect()->route('seccions.index')->with('status', 'Seccion created!');
    }

    public function edit(Request $request, Seccion $seccion)
    {
        return view('seccions.edit', compact('seccion'));
    }

    public function update(SeccionPostRequest $request, Seccion $seccion)
    {
        $data = $request->validated();
        $seccion->fill($data);
        $seccion->save();
        return redirect()->route('seccions.index')->with('status', 'Seccion updated!');
    }

    public function destroy(Request $request, Seccion $seccion)
    {
        $seccion->delete();
        return redirect()->route('seccions.index')->with('status', 'Seccion destroyed!');
    }
}
