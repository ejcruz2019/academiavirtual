<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\ContratoPostRequest;
use App\Http\Controllers\Controller;
use App\Http\Models\Contrato\Contrato;

class ContratoController extends Controller
{


    public function index()
    {
        return Contrato::all();
    }

    public function show(Request $request, Contrato $contrato)
    {
        return $contrato;
    }

    public function store(ContratoPostRequest $request)
    {
        $data = $request->validated();
        $contrato = Contrato::create($data);
        return $contrato;
    }

    public function update(ContratoPostRequest $request, Contrato $contrato)
    {
        $data = $request->validated();
        $contrato->fill($data);
        $contrato->save();

        return $contrato;
    }

    public function destroy(Request $request, Contrato $contrato)
    {
        $contrato->delete();
        return $contrato;
    }

}
