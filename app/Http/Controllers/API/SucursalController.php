<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\SucursalPostRequest;
use App\Http\Controllers\Controller;
use App\Http\Models\Sucursal\Sucursal;

class SucursalController extends Controller
{


    public function index()
    {
        return Sucursal::all();
    }

    public function show(Request $request, Sucursal $sucursal)
    {
        return $sucursal;
    }

    public function store(SucursalPostRequest $request)
    {
        $data = $request->validated();
        $sucursal = Sucursal::create($data);
        return $sucursal;
    }

    public function update(SucursalPostRequest $request, Sucursal $sucursal)
    {
        $data = $request->validated();
        $sucursal->fill($data);
        $sucursal->save();

        return $sucursal;
    }

    public function destroy(Request $request, Sucursal $sucursal)
    {
        $sucursal->delete();
        return $sucursal;
    }

}
