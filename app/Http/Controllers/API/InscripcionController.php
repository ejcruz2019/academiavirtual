<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\InscripcionPostRequest;
use App\Http\Controllers\Controller;
use App\Http\Models\Inscripcion\Inscripcion;

class InscripcionController extends Controller
{


    public function index()
    {
        return Inscripcion::all();
    }

    public function show(Request $request, Inscripcion $inscripcion)
    {
        return $inscripcion;
    }

    public function store(InscripcionPostRequest $request)
    {
        $data = $request->validated();
        $inscripcion = Inscripcion::create($data);
        return $inscripcion;
    }

    public function update(InscripcionPostRequest $request, Inscripcion $inscripcion)
    {
        $data = $request->validated();
        $inscripcion->fill($data);
        $inscripcion->save();

        return $inscripcion;
    }

    public function destroy(Request $request, Inscripcion $inscripcion)
    {
        $inscripcion->delete();
        return $inscripcion;
    }

}
