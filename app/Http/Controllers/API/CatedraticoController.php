<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\CatedraticoPostRequest;
use App\Http\Controllers\Controller;
use App\Http\Models\Catedratico\Catedratico;

class CatedraticoController extends Controller
{


    public function index()
    {
        return Catedratico::all();
    }

    public function show(Request $request, Catedratico $catedratico)
    {
        return $catedratico;
    }

    public function store(CatedraticoPostRequest $request)
    {
        $data = $request->validated();
        $catedratico = Catedratico::create($data);
        return $catedratico;
    }

    public function update(CatedraticoPostRequest $request, Catedratico $catedratico)
    {
        $data = $request->validated();
        $catedratico->fill($data);
        $catedratico->save();

        return $catedratico;
    }

    public function destroy(Request $request, Catedratico $catedratico)
    {
        $catedratico->delete();
        return $catedratico;
    }

}
