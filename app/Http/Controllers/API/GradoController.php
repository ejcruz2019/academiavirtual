<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\GradoPostRequest;
use App\Http\Controllers\Controller;
use App\Http\Models\Grado\Grado;

class GradoController extends Controller
{


    public function index()
    {
        return Grado::all();
    }

    public function show(Request $request, Grado $grado)
    {
        return $grado;
    }

    public function store(GradoPostRequest $request)
    {
        $data = $request->validated();
        $grado = Grado::create($data);
        return $grado;
    }

    public function update(GradoPostRequest $request, Grado $grado)
    {
        $data = $request->validated();
        $grado->fill($data);
        $grado->save();

        return $grado;
    }

    public function destroy(Request $request, Grado $grado)
    {
        $grado->delete();
        return $grado;
    }

}
