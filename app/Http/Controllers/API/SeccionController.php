<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\SeccionPostRequest;
use App\Http\Controllers\Controller;
use App\Http\Models\Seccion\Seccion;

class SeccionController extends Controller
{


    public function index()
    {
        return Seccion::all();
    }

    public function show(Request $request, Seccion $seccion)
    {
        return $seccion;
    }

    public function store(SeccionPostRequest $request)
    {
        $data = $request->validated();
        $seccion = Seccion::create($data);
        return $seccion;
    }

    public function update(SeccionPostRequest $request, Seccion $seccion)
    {
        $data = $request->validated();
        $seccion->fill($data);
        $seccion->save();

        return $seccion;
    }

    public function destroy(Request $request, Seccion $seccion)
    {
        $seccion->delete();
        return $seccion;
    }

}
