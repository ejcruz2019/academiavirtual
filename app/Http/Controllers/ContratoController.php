<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContratoPostRequest;
use App\Http\Models\Contrato\Contrato;


class ContratoController extends Controller
{

    public function index()
    {
        $contratos = Contrato::all();
        return view('contratos.index', compact('contratos'));
    }

    public function show(Request $request, Contrato $contrato)
    {
        return view('contratos.show', compact('contrato'));
    }

    public function create()
    {
        return view('contratos.create');
    }

    public function store(ContratoPostRequest $request)
    {
        $data = $request->validated();
        $contrato = Contrato::create($data);
        return redirect()->route('contratos.index')->with('status', 'Contrato created!');
    }

    public function edit(Request $request, Contrato $contrato)
    {
        return view('contratos.edit', compact('contrato'));
    }

    public function update(ContratoPostRequest $request, Contrato $contrato)
    {
        $data = $request->validated();
        $contrato->fill($data);
        $contrato->save();
        return redirect()->route('contratos.index')->with('status', 'Contrato updated!');
    }

    public function destroy(Request $request, Contrato $contrato)
    {
        $contrato->delete();
        return redirect()->route('contratos.index')->with('status', 'Contrato destroyed!');
    }
}
