<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GradoPostRequest;
use App\Http\Models\Grado\Grado;


class GradoController extends Controller
{

    public function index()
    {
        $grados = Grado::all();
        return view('grados.index', compact('grados'));
    }

    public function show(Request $request, Grado $grado)
    {
        return view('grados.show', compact('grado'));
    }

    public function create()
    {
        return view('grados.create');
    }

    public function store(GradoPostRequest $request)
    {
        $data = $request->validated();
        $grado = Grado::create($data);
        return redirect()->route('grados.index')->with('status', 'Grado created!');
    }

    public function edit(Request $request, Grado $grado)
    {
        return view('grados.edit', compact('grado'));
    }

    public function update(GradoPostRequest $request, Grado $grado)
    {
        $data = $request->validated();
        $grado->fill($data);
        $grado->save();
        return redirect()->route('grados.index')->with('status', 'Grado updated!');
    }

    public function destroy(Request $request, Grado $grado)
    {
        $grado->delete();
        return redirect()->route('grados.index')->with('status', 'Grado destroyed!');
    }
}
