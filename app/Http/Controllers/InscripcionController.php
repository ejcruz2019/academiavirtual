<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\InscripcionPostRequest;
use App\Http\Models\Inscripcion\Inscripcion;


class InscripcionController extends Controller
{

    public function index()
    {
        $inscripcions = Inscripcion::all();
        return view('inscripcions.index', compact('inscripcions'));
    }

    public function show(Request $request, Inscripcion $inscripcion)
    {
        return view('inscripcions.show', compact('inscripcion'));
    }

    public function create()
    {
        return view('inscripcions.create');
    }

    public function store(InscripcionPostRequest $request)
    {
        $data = $request->validated();
        $inscripcion = Inscripcion::create($data);
        return redirect()->route('inscripcions.index')->with('status', 'Inscripcion created!');
    }

    public function edit(Request $request, Inscripcion $inscripcion)
    {
        return view('inscripcions.edit', compact('inscripcion'));
    }

    public function update(InscripcionPostRequest $request, Inscripcion $inscripcion)
    {
        $data = $request->validated();
        $inscripcion->fill($data);
        $inscripcion->save();
        return redirect()->route('inscripcions.index')->with('status', 'Inscripcion updated!');
    }

    public function destroy(Request $request, Inscripcion $inscripcion)
    {
        $inscripcion->delete();
        return redirect()->route('inscripcions.index')->with('status', 'Inscripcion destroyed!');
    }
}
