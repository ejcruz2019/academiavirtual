<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CatedraticoPostRequest;
use App\Http\Models\Catedratico\Catedratico;


class CatedraticoController extends Controller
{

    public function index()
    {
        $catedraticos = Catedratico::all();
        return view('catedraticos.index', compact('catedraticos'));
    }

    public function show(Request $request, Catedratico $catedratico)
    {
        return view('catedraticos.show', compact('catedratico'));
    }

    public function create()
    {
        return view('catedraticos.create');
    }

    public function store(CatedraticoPostRequest $request)
    {
        $data = $request->validated();
        $catedratico = Catedratico::create($data);
        return redirect()->route('catedraticos.index')->with('status', 'Catedratico created!');
    }

    public function edit(Request $request, Catedratico $catedratico)
    {
        return view('catedraticos.edit', compact('catedratico'));
    }

    public function update(CatedraticoPostRequest $request, Catedratico $catedratico)
    {
        $data = $request->validated();
        $catedratico->fill($data);
        $catedratico->save();
        return redirect()->route('catedraticos.index')->with('status', 'Catedratico updated!');
    }

    public function destroy(Request $request, Catedratico $catedratico)
    {
        $catedratico->delete();
        return redirect()->route('catedraticos.index')->with('status', 'Catedratico destroyed!');
    }
}
