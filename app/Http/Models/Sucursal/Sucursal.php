<?php

namespace App\Http\Models\Sucursal;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = 'sucursal';
    public $timestamps = false;

    protected $fillable = [
        'id_sucursal','nombre','razon_social','direccion','telefono','correo'
    ];

    protected $primaryKey = 'id_sucursal';
}
