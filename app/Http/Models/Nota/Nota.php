<?php

namespace App\Http\Models\Nota;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    protected $table = 'nota';
    public $timestamps = false;

    protected $fillable = [
        'id_nota','fecha','nota','inscripcion_id_inscripcion'
    ];

    protected $primaryKey = 'id_nota';
}
