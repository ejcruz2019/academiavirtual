<?php

namespace App\Http\Models\Contrato;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    protected $table = 'contrato';
    public $timestamps = false;

    protected $fillable = [
        'id_contrato','fecha','seccion_id_seccion','catedratico_id_catedratico'
    ];

    protected $primaryKey = 'id_contrato';
}
