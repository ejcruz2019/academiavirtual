<?php

namespace App\Http\Models\Catedratico;

use Illuminate\Database\Eloquent\Model;

class Catedratico extends Model
{
    protected $table = 'catedratico';
    public $timestamps = false;

    protected $fillable = [
        'id_catedratico','nombre','apellido','dpi','telefono','correo'
    ];

    protected $primaryKey = 'id_catedratico';
}
