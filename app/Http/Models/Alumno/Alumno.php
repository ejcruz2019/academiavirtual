<?php

namespace App\Http\Models\Alumno;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $table = 'alumno';
    public $timestamps = false;

    protected $fillable = [
        'id_alumno','apellido','nombre','dpi','telefono','correo'
    ];

    protected $primaryKey = 'id_alumno';
}
