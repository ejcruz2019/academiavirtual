<?php

namespace App\Http\Models\Seccion;

use Illuminate\Database\Eloquent\Model;

class Seccion extends Model
{
    protected $table = 'seccion';
    public $timestamps = false;

    protected $fillable = [
        'id_seccion','nombre_seccion','grado_id_grado'
    ];

    protected $primaryKey = 'id_seccion';
}
