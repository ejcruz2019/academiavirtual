<?php

namespace App\Http\Models\Grado;

use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{
    protected $table = 'grado';
    public $timestamps = false;

    protected $fillable = [
        'id_grado','nombre_grado'
    ];

    protected $primaryKey = 'id_grado';
}
