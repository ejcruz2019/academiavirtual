<?php

namespace App\Http\Models\User;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'user';
    public $timestamps = false;

    protected $fillable = [
        'id','name','email','email_verifiied_at','password','remember_token','created_at','updated_at',
        'tipousuario_idtipo_usuario','sucursal_id_sucursal',
    ];

    protected $primaryKey = 'id';
}
