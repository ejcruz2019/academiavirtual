<?php

namespace App\Http\Models\Inscripcion;

use Illuminate\Database\Eloquent\Model;

class Inscripcion extends Model
{
    protected $table = 'inscripcion';
    public $timestamps = false;

    protected $fillable = [
        'id_inscripcion','fecha','user_id','alumno_id_alumno','seccion_id_seccion'
    ];

    protected $primaryKey = 'id_inscripcion';
}
