<?php

namespace App\Http\Models\TipoUsuario;

use Illuminate\Database\Eloquent\Model;

class TipoUsuario extends Model
{
    protected $table = 'tipousuario';
    public $timestamps = false;

    protected $fillable = [
        'idtipo_usuario','tipo'
    ];

    protected $primaryKey = 'idtipo_usuario';
}
