<?php

return [
    'singular' => 'Catedratico',
    'plural' => 'Catedraticos',
//Fields
    'id_catedratico' => 'Id Catedratico',
    'nombre' => 'Nombre',
    'apellido' => 'Apellido',
    'dpi' => 'Dpi',
    'telefono' => 'Telefono',
    'correo' => 'Correo',
//Relations
//Custom
];