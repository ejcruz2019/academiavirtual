<?php

return [
    'singular' => 'Alumno',
    'plural' => 'Alumnos',
//Fields
    'id_alumno' => 'Id Alumno',
    'apellido' => 'Apellido',
    'nombre' => 'Nombre',
    'dpi' => 'Dpi',
    'telefono' => 'Telefono',
    'correo' => 'Correo',
//Relations
//Custom
];