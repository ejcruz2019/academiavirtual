<?php

return [
    'singular' => 'Sucursal',
    'plural' => 'Sucursals',
//Fields
    'id_sucursal' => 'Id Sucursal',
    'nombre' => 'Nombre',
    'razon_social' => 'Razon Social',
    'direccion' => 'Direccion',
    'telefono' => 'Telefono',
    'correo' => 'Correo',
//Relations
//Custom
];