<?php

return [
    'singular' => 'Nota',
    'plural' => 'Notas',
//Fields
    'id_nota' => 'Id Nota',
    'inscripcion_id_inscripcion' => 'Inscripcion Id Inscripcion',
    'fecha' => 'Fecha',
    'nota' => 'Nota',
//Relations
//Custom
];