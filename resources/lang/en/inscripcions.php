<?php

return [
    'singular' => 'Inscripcion',
    'plural' => 'Inscripcions',
//Fields
    'id_inscripcion' => 'Id Inscripcion',
    'user_id' => 'User Id',
    'alumno_id_alumno' => 'Alumno Id Alumno',
    'seccion_id_seccion' => 'Seccion Id Seccion',
    'fecha' => 'Fecha',
//Relations
//Custom
];