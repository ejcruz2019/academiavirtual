<?php

return [
    'singular' => 'Contrato',
    'plural' => 'Contratos',
//Fields
    'id_contrato' => 'Id Contrato',
    'seccion_id_seccion' => 'Seccion Id Seccion',
    'catedratico_id_catedratico' => 'Catedratico Id Catedratico',
    'fecha' => 'Fecha',
//Relations
//Custom
];