@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">

        <div class="card-header">
            <h1> Catedratico Create </h1>
        </div>
        <div class="card-body">

        @if($errors->any())
        <ul>
            @foreach($errors->all() as $error)
            <li class="text-danger">{{ $error }}</li>
            @endforeach
        </ul @endif <form action="{{route('catedraticos.store')}}" method="POST" novalidate>
        @csrf
        
                        <div class="form-group">
            <label for="id_catedratico">Id Catedratico</label>
                        <input class="form-control Integer"  type="number"  name="id_catedratico" id="id_catedratico" value="{{old('id_catedratico')}}"                         required="required"
                        >
                        @if($errors->has('id_catedratico'))
            <p class="text-danger">{{$errors->first('id_catedratico')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="nombre">Nombre</label>
                        <input class="form-control String"  type="text"  name="nombre" id="nombre" value="{{old('nombre')}}"             maxlength="50"
                                    required="required"
                        >
                        @if($errors->has('nombre'))
            <p class="text-danger">{{$errors->first('nombre')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="apellido">Apellido</label>
                        <input class="form-control String"  type="text"  name="apellido" id="apellido" value="{{old('apellido')}}"             maxlength="50"
                                    required="required"
                        >
                        @if($errors->has('apellido'))
            <p class="text-danger">{{$errors->first('apellido')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="dpi">Dpi</label>
                        <input class="form-control String"  type="text"  name="dpi" id="dpi" value="{{old('dpi')}}"             maxlength="13"
                                    required="required"
                        >
                        @if($errors->has('dpi'))
            <p class="text-danger">{{$errors->first('dpi')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="telefono">Telefono</label>
                        <input class="form-control String"  type="text"  name="telefono" id="telefono" value="{{old('telefono')}}"             maxlength="10"
                                    required="required"
                        >
                        @if($errors->has('telefono'))
            <p class="text-danger">{{$errors->first('telefono')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="correo">Correo</label>
                        <input class="form-control String"  type="text"  name="correo" id="correo" value="{{old('correo')}}"             maxlength="125"
                                    required="required"
                        >
                        @if($errors->has('correo'))
            <p class="text-danger">{{$errors->first('correo')}}</p>
            @endif
        </div>
                        <div>
            <button class="btn btn-primary" type="submit">Create</button>
            <a href="{{ url()->previous() }}">Back</a>
        </div>
        </form>
        </div>
    </div>
</div>
@endsection