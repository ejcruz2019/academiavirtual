@extends('layouts.app')
@section('content')
<div class="container">

    <div class="card mb-4">

        <div class="card-header">
            <h1> Catedratico Show </h1>
        </div>

    <div class="card-body">
                        <div class="form-group">
            <label class="col-form-label" for="value">Id Catedratico</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$catedratico->id_catedratico}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Nombre</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$catedratico->nombre}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Apellido</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$catedratico->apellido}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Dpi</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$catedratico->dpi}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Telefono</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$catedratico->telefono}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Correo</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$catedratico->correo}}">
        </div>
                    </div>

    </div>

    <div class="card mb-4">

        
    </div>



    <a href="{{ url()->previous() }}">Back</a>
</div>
@endsection