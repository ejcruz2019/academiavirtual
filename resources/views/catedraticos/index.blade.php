@extends('layouts.app')
@section('content')
<div class="container">

    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h1> Catedraticos </h1>
        </div>
    <div class="card-body">

    <div>
        <a href="{{route('catedraticos.create')}}">New</a>
    </div>
    <table class="table table-striped">
        @if(count($catedraticos))
        <thead>
            <tr>
                <th>&nbsp;</th>
                                                <td>Id Catedratico</td>
                
                                                <td>Nombre</td>
                
                                                <td>Apellido</td>
                
                                                <td>Dpi</td>
                
                                                <td>Telefono</td>
                
                                                <td>Correo</td>
                
                            </tr>

        </thead>
        @endif
        <tbody>
            @forelse($catedraticos as $catedratico)
            <tr>
                <td>
                    <a href="{{route('catedraticos.show',['catedratico'=>$catedratico] )}}">Show</a>
                    <a href="{{route('catedraticos.edit',['catedratico'=>$catedratico] )}}">Edit</a>
                    <a href="javascript:void(0)" onclick="event.preventDefault();
                    document.getElementById('delete-catedratico-{{$catedratico->id}}').submit();">
                        Delete
                    </a>
                    <form id="delete-catedratico-{{$catedratico->id}}" action="{{route('catedraticos.destroy',['catedratico'=>$catedratico])}}" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                </td>
                                                <td>{{$catedratico->id_catedratico}}</td>
                                                                <td>{{$catedratico->nombre}}</td>
                                                                <td>{{$catedratico->apellido}}</td>
                                                                <td>{{$catedratico->dpi}}</td>
                                                                <td>{{$catedratico->telefono}}</td>
                                                                <td>{{$catedratico->correo}}</td>
                                
            </tr>
            @empty
            <p>No Catedraticos</p>
            @endforelse
        </tbody>
    </table>
    </div>
    </div>

</div>

@endsection