@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">

        <div class="card-header">
            <h1> Inscripcion Create </h1>
        </div>
        <div class="card-body">

        @if($errors->any())
        <ul>
            @foreach($errors->all() as $error)
            <li class="text-danger">{{ $error }}</li>
            @endforeach
        </ul @endif <form action="{{route('inscripcions.store')}}" method="POST" novalidate>
        @csrf
        
                        <div class="form-group">
            <label for="id_inscripcion">Id Inscripcion</label>
                        <input class="form-control Integer"  type="number"  name="id_inscripcion" id="id_inscripcion" value="{{old('id_inscripcion')}}"                         required="required"
                        >
                        @if($errors->has('id_inscripcion'))
            <p class="text-danger">{{$errors->first('id_inscripcion')}}</p>
            @endif
        </div>
                                                <div class="form-group">
            <label for="alumno_id_alumno">Alumno Id Alumno</label>
                        <input class="form-control Integer"  type="number"  name="alumno_id_alumno" id="alumno_id_alumno" value="{{old('alumno_id_alumno')}}"                         required="required"
                        >
                        @if($errors->has('alumno_id_alumno'))
            <p class="text-danger">{{$errors->first('alumno_id_alumno')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="seccion_id_seccion">Seccion Id Seccion</label>
                        <input class="form-control Integer"  type="number"  name="seccion_id_seccion" id="seccion_id_seccion" value="{{old('seccion_id_seccion')}}"                         required="required"
                        >
                        @if($errors->has('seccion_id_seccion'))
            <p class="text-danger">{{$errors->first('seccion_id_seccion')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="fecha">Fecha</label>
                        <input class="form-control Date"  type="date"  name="fecha" id="fecha" value="{{old('fecha')}}"                         required="required"
                        >
                        @if($errors->has('fecha'))
            <p class="text-danger">{{$errors->first('fecha')}}</p>
            @endif
        </div>
                        <div>
            <button class="btn btn-primary" type="submit">Create</button>
            <a href="{{ url()->previous() }}">Back</a>
        </div>
        </form>
        </div>
    </div>
</div>
@endsection