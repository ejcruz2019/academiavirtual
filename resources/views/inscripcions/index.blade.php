@extends('layouts.app')
@section('content')
<div class="container">

    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h1> Inscripcions </h1>
        </div>
    <div class="card-body">

    <div>
        <a href="{{route('inscripcions.create')}}">New</a>
    </div>
    <table class="table table-striped">
        @if(count($inscripcions))
        <thead>
            <tr>
                <th>&nbsp;</th>
                                                <td>Id Inscripcion</td>
                
                                
                                                <td>Alumno Id Alumno</td>
                
                                                <td>Seccion Id Seccion</td>
                
                                                <td>Fecha</td>
                
                            </tr>

        </thead>
        @endif
        <tbody>
            @forelse($inscripcions as $inscripcion)
            <tr>
                <td>
                    <a href="{{route('inscripcions.show',['inscripcion'=>$inscripcion] )}}">Show</a>
                    <a href="{{route('inscripcions.edit',['inscripcion'=>$inscripcion] )}}">Edit</a>
                    <a href="javascript:void(0)" onclick="event.preventDefault();
                    document.getElementById('delete-inscripcion-{{$inscripcion->id}}').submit();">
                        Delete
                    </a>
                    <form id="delete-inscripcion-{{$inscripcion->id}}" action="{{route('inscripcions.destroy',['inscripcion'=>$inscripcion])}}" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                </td>
                                                <td>{{$inscripcion->id_inscripcion}}</td>
                                                                                                <td>{{$inscripcion->alumno_id_alumno}}</td>
                                                                <td>{{$inscripcion->seccion_id_seccion}}</td>
                                                                <td>{{$inscripcion->fecha}}</td>
                                
            </tr>
            @empty
            <p>No Inscripcions</p>
            @endforelse
        </tbody>
    </table>
    </div>
    </div>

</div>

@endsection