@extends('layouts.navbar')
@section('contenido')
<div class="container">
    <div class="card card-primary">

        <div class="card-header ">
            <h3> Nuevo Alumno </h3>
        </div>
        <div class="card-body">

        @if($errors->any())
        <ul>
            @foreach($errors->all() as $error)
            <li class="text-danger">{{ $error }}</li>
            @endforeach
        </ul @endif <form action="{{route('alumnos.store')}}" method="POST" novalidate>
        @csrf

                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input class="form-control String"  type="text"  name="nombre" id="nombre" value="{{old('nombre')}}"             maxlength="50"
                           required="required" placeholder="Ingresar nombre"
                    >
                    @if($errors->has('nombre'))
                        <p class="text-danger">{{$errors->first('nombre')}}</p>
                    @endif
                </div>


                                <div class="form-group">
            <label for="apellido">Apellido</label>
                        <input class="form-control String"  type="text"  name="apellido" id="apellido" value="{{old('apellido')}}"             maxlength="50"
                                    required="required" placeholder="Ingresar apellido"
                        >
                        @if($errors->has('apellido'))
            <p class="text-danger">{{$errors->first('apellido')}}</p>
            @endif
        </div>

                                <div class="form-group">
            <label for="dpi">DPI</label>
                        <input class="form-control String"  type="text"  name="dpi" id="dpi" value="{{old('dpi')}}"             maxlength="13"
                                    required="required" placeholder="Ingresar DPI"
                        >
                        @if($errors->has('dpi'))
            <p class="text-danger">{{$errors->first('dpi')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="telefono">Telefono o Celular</label>
                        <input class="form-control String"  type="text"  name="telefono" id="telefono" value="{{old('telefono')}}"             maxlength="10"
                                    required="required" placeholder="Ingresar telefono o celular"
                        >
                        @if($errors->has('telefono'))
            <p class="text-danger">{{$errors->first('telefono')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="correo">Correo</label>
                        <input class="form-control String"  type="text"  name="correo" id="correo" value="{{old('correo')}}"             maxlength="125"
                                    required="required" placeholder="Ingresar correo electronico"
                        >
                        @if($errors->has('correo'))
            <p class="text-danger">{{$errors->first('correo')}}</p>
            @endif
        </div>
                        <div>
            <button class="btn btn-primary" type="submit">Guardar</button>
            <a href="{{ url()->previous() }}">Regresar</a>
        </div>
        </form>
        </div>
    </div>
</div>
@endsection
