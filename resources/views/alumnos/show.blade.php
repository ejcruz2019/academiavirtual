@extends('layouts.app')
@section('content')
<div class="container">

    <div class="card mb-4">

        <div class="card-header">
            <h1> Alumno Show </h1>
        </div>

    <div class="card-body">
                        <div class="form-group">
            <label class="col-form-label" for="value">Id Alumno</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$alumno->id_alumno}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Apellido</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$alumno->apellido}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Nombre</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$alumno->nombre}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Dpi</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$alumno->dpi}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Telefono</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$alumno->telefono}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Correo</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$alumno->correo}}">
        </div>
                    </div>

    </div>

    <div class="card mb-4">

        
    </div>



    <a href="{{ url()->previous() }}">Back</a>
</div>
@endsection