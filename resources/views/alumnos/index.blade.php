@extends('layouts.navbar')
@section('contenido')


<div class="container">



    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card ">
        <div class="card-header bg-gradient-blue">
            <h1> Alumnos </h1>
        </div>
    <div class="card-body">

    <div class="btn bg-cyan ">
        <a href="{{route('alumnos.create')}}" class="text-white">Nuevo Alumno</a>
    </div>
        <p></p>
        <div class="dropdown-divider"></div>
    <table id="alumnosTB" class="table table-bordered table-striped">
        @if(count($alumnos))
        <thead>
            <tr class="text-sm-center bg-teal">

                                                <td>Visualizar</td>
                <td>Modificar</td>
                <td>Eliminar</td>
                                                <td>ID</td>

                                                <td>Apellido</td>

                                                <td>Nombre</td>

                                                <td>DPI</td>

                                                <td>Telefono</td>

                                                <td>Correo</td>
            </tr>

        </thead>
        @endif
        <tbody>
            @forelse($alumnos as $alumno)

                <td>
                    <a href="{{route('alumnos.show',['alumno'=>$alumno] )}}" class="btn btn-primary">Mostrar</a>
                </td>
                <td>
                    <a href="{{route('alumnos.edit',['alumno'=>$alumno] )}}" class="btn btn-warning">Editar</a>
                </td>
                <td>
                    <a href="javascript:void(0)" onclick="event.preventDefault();
                    document.getElementById('delete-alumno-{{$alumno->id}}').submit();" class="btn btn-danger">
                        Eliminar
                    </a>
                    <form id="delete-alumno-{{$alumno->id}}" action="{{route('alumnos.destroy',['alumno'=>$alumno])}}" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                </td>
                                                                <td>{{$alumno->id_alumno}}</td>
                                                                <td>{{$alumno->apellido}}</td>
                                                                <td>{{$alumno->nombre}}</td>
                                                                <td>{{$alumno->dpi}}</td>
                                                                <td>{{$alumno->telefono}}</td>
                                                                <td>{{$alumno->correo}}</td>


            @empty
            <div class="text-sm-center">
            <h1>Aun no has registrado un Alumno Nuevo</h1>
            </div>
            @endforelse
        </tbody>
    </table>
    </div>
    </div>

</div>

<!-- DataTables -->
<script src="{!! asset('AdminLTE-3.0.5/plugins/datatables/jquery.dataTables.min.js') !!}" async></script>
<script src="{!! asset('AdminLTE-3.0.5/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') !!}" async></script>
<script src="{!! asset('AdminLTE-3.0.5/plugins/datatables-responsive/js/dataTables.responsive.min.js') !!}" async></script>
<script src="{!! asset(('AdminLTE-3.0.5/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')) !!}" async></script>

<script>
    $(function () {
        $("#alumnosTB").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
        $('#alumnosTB').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
        });
    });
</script>

@endsection
