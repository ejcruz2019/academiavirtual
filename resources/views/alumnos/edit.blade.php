@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">

        <div class="card-header">
            <h1> Alumno Edit </h1>
        </div>
        <div class="card-body">

    @if($errors->any())
    <ul>
        @foreach($errors->all() as $error)
        <li class="text-danger">{{ $error }}</li>
        @endforeach
    </ul>

    @endif

    <form action="{{route('alumnos.update',['alumno'=>$alumno->id_alumno])}}" method="POST" novalidate>
        @csrf
        @method('PUT')


                        <div class="form-group">
            <label for="id_alumno">Id Alumno</label>
                    <input class="form-control Integer"  type="number"  name="id_alumno" id="id_alumno" value="{{old('id_alumno',$alumno->id_alumno)}}"
                                    required="required"
                        >
                    @if($errors->has('id_alumno'))
            <p class="text-danger">{{$errors->first('id_alumno')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="apellido">Apellido</label>
                    <input class="form-control String"  type="text"  name="apellido" id="apellido" value="{{old('apellido',$alumno->apellido)}}"
                                    required="required"
                        >
                    @if($errors->has('apellido'))
            <p class="text-danger">{{$errors->first('apellido')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="nombre">Nombre</label>
                    <input class="form-control String"  type="text"  name="nombre" id="nombre" value="{{old('nombre',$alumno->nombre)}}"
                                    required="required"
                        >
                    @if($errors->has('nombre'))
            <p class="text-danger">{{$errors->first('nombre')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="dpi">Dpi</label>
                    <input class="form-control String"  type="text"  name="dpi" id="dpi" value="{{old('dpi',$alumno->dpi)}}"
                                    required="required"
                        >
                    @if($errors->has('dpi'))
            <p class="text-danger">{{$errors->first('dpi')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="telefono">Telefono</label>
                    <input class="form-control String"  type="text"  name="telefono" id="telefono" value="{{old('telefono',$alumno->telefono)}}"
                                    required="required"
                        >
                    @if($errors->has('telefono'))
            <p class="text-danger">{{$errors->first('telefono')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="correo">Correo</label>
                    <input class="form-control String"  type="text"  name="correo" id="correo" value="{{old('correo',$alumno->correo)}}"
                                    required="required"
                        >
                    @if($errors->has('correo'))
            <p class="text-danger">{{$errors->first('correo')}}</p>
            @endif
        </div>
                        <div>
            <button class="btn btn-primary" type="submit">Save</button>
            <a href="{{ url()->previous() }}">Back</a>
        </div>
    </form>
    </div>
        </div>

</div>
@endsection
