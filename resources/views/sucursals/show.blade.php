@extends('layouts.app')
@section('content')
<div class="container">

    <div class="card mb-4">

        <div class="card-header">
            <h1> Sucursal Show </h1>
        </div>

    <div class="card-body">
                        <div class="form-group">
            <label class="col-form-label" for="value">Id Sucursal</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$sucursal->id_sucursal}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Nombre</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$sucursal->nombre}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Razon Social</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$sucursal->razon_social}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Direccion</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$sucursal->direccion}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Telefono</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$sucursal->telefono}}">
        </div>
                                <div class="form-group">
            <label class="col-form-label" for="value">Correo</label>
            <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$sucursal->correo}}">
        </div>
                    </div>

    </div>

    <div class="card mb-4">

        
    </div>



    <a href="{{ url()->previous() }}">Back</a>
</div>
@endsection