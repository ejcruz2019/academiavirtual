@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">

        <div class="card-header">
            <h1> Sucursal Create </h1>
        </div>
        <div class="card-body">

        @if($errors->any())
        <ul>
            @foreach($errors->all() as $error)
            <li class="text-danger">{{ $error }}</li>
            @endforeach
        </ul @endif <form action="{{route('sucursals.store')}}" method="POST" novalidate>
        @csrf
        
                        <div class="form-group">
            <label for="id_sucursal">Id Sucursal</label>
                        <input class="form-control Integer"  type="number"  name="id_sucursal" id="id_sucursal" value="{{old('id_sucursal')}}"                         required="required"
                        >
                        @if($errors->has('id_sucursal'))
            <p class="text-danger">{{$errors->first('id_sucursal')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="nombre">Nombre</label>
                        <input class="form-control String"  type="text"  name="nombre" id="nombre" value="{{old('nombre')}}"             maxlength="45"
                                    required="required"
                        >
                        @if($errors->has('nombre'))
            <p class="text-danger">{{$errors->first('nombre')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="razon_social">Razon Social</label>
                        <input class="form-control String"  type="text"  name="razon_social" id="razon_social" value="{{old('razon_social')}}"             maxlength="45"
                                    required="required"
                        >
                        @if($errors->has('razon_social'))
            <p class="text-danger">{{$errors->first('razon_social')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="direccion">Direccion</label>
                        <input class="form-control String"  type="text"  name="direccion" id="direccion" value="{{old('direccion')}}"             maxlength="125"
                                    required="required"
                        >
                        @if($errors->has('direccion'))
            <p class="text-danger">{{$errors->first('direccion')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="telefono">Telefono</label>
                        <input class="form-control String"  type="text"  name="telefono" id="telefono" value="{{old('telefono')}}"             maxlength="10"
                                    required="required"
                        >
                        @if($errors->has('telefono'))
            <p class="text-danger">{{$errors->first('telefono')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="correo">Correo</label>
                        <input class="form-control String"  type="text"  name="correo" id="correo" value="{{old('correo')}}"             maxlength="125"
                                    required="required"
                        >
                        @if($errors->has('correo'))
            <p class="text-danger">{{$errors->first('correo')}}</p>
            @endif
        </div>
                        <div>
            <button class="btn btn-primary" type="submit">Create</button>
            <a href="{{ url()->previous() }}">Back</a>
        </div>
        </form>
        </div>
    </div>
</div>
@endsection