@extends('layouts.app')
@section('content')
<div class="container">

    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h1> Sucursals </h1>
        </div>
    <div class="card-body">

    <div>
        <a href="{{route('sucursals.create')}}">New</a>
    </div>
    <table class="table table-striped">
        @if(count($sucursals))
        <thead>
            <tr>
                <th>&nbsp;</th>
                                                <td>Id Sucursal</td>
                
                                                <td>Nombre</td>
                
                                                <td>Razon Social</td>
                
                                                <td>Direccion</td>
                
                                                <td>Telefono</td>
                
                                                <td>Correo</td>
                
                            </tr>

        </thead>
        @endif
        <tbody>
            @forelse($sucursals as $sucursal)
            <tr>
                <td>
                    <a href="{{route('sucursals.show',['sucursal'=>$sucursal] )}}">Show</a>
                    <a href="{{route('sucursals.edit',['sucursal'=>$sucursal] )}}">Edit</a>
                    <a href="javascript:void(0)" onclick="event.preventDefault();
                    document.getElementById('delete-sucursal-{{$sucursal->id}}').submit();">
                        Delete
                    </a>
                    <form id="delete-sucursal-{{$sucursal->id}}" action="{{route('sucursals.destroy',['sucursal'=>$sucursal])}}" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                </td>
                                                <td>{{$sucursal->id_sucursal}}</td>
                                                                <td>{{$sucursal->nombre}}</td>
                                                                <td>{{$sucursal->razon_social}}</td>
                                                                <td>{{$sucursal->direccion}}</td>
                                                                <td>{{$sucursal->telefono}}</td>
                                                                <td>{{$sucursal->correo}}</td>
                                
            </tr>
            @empty
            <p>No Sucursals</p>
            @endforelse
        </tbody>
    </table>
    </div>
    </div>

</div>

@endsection