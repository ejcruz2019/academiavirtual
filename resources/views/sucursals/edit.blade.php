@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">

        <div class="card-header">
            <h1> Sucursal Edit </h1>
        </div>
        <div class="card-body">

    @if($errors->any())
    <ul>
        @foreach($errors->all() as $error)
        <li class="text-danger">{{ $error }}</li>
        @endforeach
    </ul>

    @endif

    <form action="{{route('sucursals.update',['sucursal'=>$sucursal->id])}}" method="POST" novalidate>
        @csrf
        @method('PUT')
        

                        <div class="form-group">
            <label for="id_sucursal">Id Sucursal</label>
                    <input class="form-control Integer"  type="number"  name="id_sucursal" id="id_sucursal" value="{{old('id_sucursal',$sucursal->id_sucursal)}}"
                                    required="required"
                        >
                    @if($errors->has('id_sucursal'))
            <p class="text-danger">{{$errors->first('id_sucursal')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="nombre">Nombre</label>
                    <input class="form-control String"  type="text"  name="nombre" id="nombre" value="{{old('nombre',$sucursal->nombre)}}"
                                    required="required"
                        >
                    @if($errors->has('nombre'))
            <p class="text-danger">{{$errors->first('nombre')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="razon_social">Razon Social</label>
                    <input class="form-control String"  type="text"  name="razon_social" id="razon_social" value="{{old('razon_social',$sucursal->razon_social)}}"
                                    required="required"
                        >
                    @if($errors->has('razon_social'))
            <p class="text-danger">{{$errors->first('razon_social')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="direccion">Direccion</label>
                    <input class="form-control String"  type="text"  name="direccion" id="direccion" value="{{old('direccion',$sucursal->direccion)}}"
                                    required="required"
                        >
                    @if($errors->has('direccion'))
            <p class="text-danger">{{$errors->first('direccion')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="telefono">Telefono</label>
                    <input class="form-control String"  type="text"  name="telefono" id="telefono" value="{{old('telefono',$sucursal->telefono)}}"
                                    required="required"
                        >
                    @if($errors->has('telefono'))
            <p class="text-danger">{{$errors->first('telefono')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="correo">Correo</label>
                    <input class="form-control String"  type="text"  name="correo" id="correo" value="{{old('correo',$sucursal->correo)}}"
                                    required="required"
                        >
                    @if($errors->has('correo'))
            <p class="text-danger">{{$errors->first('correo')}}</p>
            @endif
        </div>
                        <div>
            <button class="btn btn-primary" type="submit">Save</button>
            <a href="{{ url()->previous() }}">Back</a>
        </div>
    </form>
    </div>
        </div>

</div>
@endsection