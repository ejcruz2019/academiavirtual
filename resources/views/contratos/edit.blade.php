@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">

        <div class="card-header">
            <h1> Contrato Edit </h1>
        </div>
        <div class="card-body">

    @if($errors->any())
    <ul>
        @foreach($errors->all() as $error)
        <li class="text-danger">{{ $error }}</li>
        @endforeach
    </ul>

    @endif

    <form action="{{route('contratos.update',['contrato'=>$contrato->id])}}" method="POST" novalidate>
        @csrf
        @method('PUT')
        

                        <div class="form-group">
            <label for="id_contrato">Id Contrato</label>
                    <input class="form-control Integer"  type="number"  name="id_contrato" id="id_contrato" value="{{old('id_contrato',$contrato->id_contrato)}}"
                                    required="required"
                        >
                    @if($errors->has('id_contrato'))
            <p class="text-danger">{{$errors->first('id_contrato')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="seccion_id_seccion">Seccion Id Seccion</label>
                    <input class="form-control Integer"  type="number"  name="seccion_id_seccion" id="seccion_id_seccion" value="{{old('seccion_id_seccion',$contrato->seccion_id_seccion)}}"
                                    required="required"
                        >
                    @if($errors->has('seccion_id_seccion'))
            <p class="text-danger">{{$errors->first('seccion_id_seccion')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="catedratico_id_catedratico">Catedratico Id Catedratico</label>
                    <input class="form-control Integer"  type="number"  name="catedratico_id_catedratico" id="catedratico_id_catedratico" value="{{old('catedratico_id_catedratico',$contrato->catedratico_id_catedratico)}}"
                                    required="required"
                        >
                    @if($errors->has('catedratico_id_catedratico'))
            <p class="text-danger">{{$errors->first('catedratico_id_catedratico')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="fecha">Fecha</label>
                    <input class="form-control Date"  type="date"  name="fecha" id="fecha" value="{{old('fecha',$contrato->fecha)}}"
                                    required="required"
                        >
                    @if($errors->has('fecha'))
            <p class="text-danger">{{$errors->first('fecha')}}</p>
            @endif
        </div>
                        <div>
            <button class="btn btn-primary" type="submit">Save</button>
            <a href="{{ url()->previous() }}">Back</a>
        </div>
    </form>
    </div>
        </div>

</div>
@endsection