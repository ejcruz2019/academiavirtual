@extends('layouts.app')
@section('content')
<div class="container">

    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h1> Contratos </h1>
        </div>
    <div class="card-body">

    <div>
        <a href="{{route('contratos.create')}}">New</a>
    </div>
    <table class="table table-striped">
        @if(count($contratos))
        <thead>
            <tr>
                <th>&nbsp;</th>
                                                <td>Id Contrato</td>
                
                                                <td>Seccion Id Seccion</td>
                
                                                <td>Catedratico Id Catedratico</td>
                
                                                <td>Fecha</td>
                
                            </tr>

        </thead>
        @endif
        <tbody>
            @forelse($contratos as $contrato)
            <tr>
                <td>
                    <a href="{{route('contratos.show',['contrato'=>$contrato] )}}">Show</a>
                    <a href="{{route('contratos.edit',['contrato'=>$contrato] )}}">Edit</a>
                    <a href="javascript:void(0)" onclick="event.preventDefault();
                    document.getElementById('delete-contrato-{{$contrato->id}}').submit();">
                        Delete
                    </a>
                    <form id="delete-contrato-{{$contrato->id}}" action="{{route('contratos.destroy',['contrato'=>$contrato])}}" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                </td>
                                                <td>{{$contrato->id_contrato}}</td>
                                                                <td>{{$contrato->seccion_id_seccion}}</td>
                                                                <td>{{$contrato->catedratico_id_catedratico}}</td>
                                                                <td>{{$contrato->fecha}}</td>
                                
            </tr>
            @empty
            <p>No Contratos</p>
            @endforelse
        </tbody>
    </table>
    </div>
    </div>

</div>

@endsection