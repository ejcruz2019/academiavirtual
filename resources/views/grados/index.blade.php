@extends('layouts.app')
@section('content')
<div class="container">

    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h1> Grados </h1>
        </div>
    <div class="card-body">

    <div>
        <a href="{{route('grados.create')}}">New</a>
    </div>
    <table class="table table-striped">
        @if(count($grados))
        <thead>
            <tr>
                <th>&nbsp;</th>
                                                <td>Id Grado</td>
                
                                                <td>Nombre Grado</td>
                
                            </tr>

        </thead>
        @endif
        <tbody>
            @forelse($grados as $grado)
            <tr>
                <td>
                    <a href="{{route('grados.show',['grado'=>$grado] )}}">Show</a>
                    <a href="{{route('grados.edit',['grado'=>$grado] )}}">Edit</a>
                    <a href="javascript:void(0)" onclick="event.preventDefault();
                    document.getElementById('delete-grado-{{$grado->id}}').submit();">
                        Delete
                    </a>
                    <form id="delete-grado-{{$grado->id}}" action="{{route('grados.destroy',['grado'=>$grado])}}" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                </td>
                                                <td>{{$grado->id_grado}}</td>
                                                                <td>{{$grado->nombre_grado}}</td>
                                
            </tr>
            @empty
            <p>No Grados</p>
            @endforelse
        </tbody>
    </table>
    </div>
    </div>

</div>

@endsection