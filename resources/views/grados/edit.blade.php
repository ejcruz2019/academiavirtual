@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">

        <div class="card-header">
            <h1> Grado Edit </h1>
        </div>
        <div class="card-body">

    @if($errors->any())
    <ul>
        @foreach($errors->all() as $error)
        <li class="text-danger">{{ $error }}</li>
        @endforeach
    </ul>

    @endif

    <form action="{{route('grados.update',['grado'=>$grado->id])}}" method="POST" novalidate>
        @csrf
        @method('PUT')
        

                        <div class="form-group">
            <label for="id_grado">Id Grado</label>
                    <input class="form-control Integer"  type="number"  name="id_grado" id="id_grado" value="{{old('id_grado',$grado->id_grado)}}"
                                    required="required"
                        >
                    @if($errors->has('id_grado'))
            <p class="text-danger">{{$errors->first('id_grado')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="nombre_grado">Nombre Grado</label>
                    <input class="form-control String"  type="text"  name="nombre_grado" id="nombre_grado" value="{{old('nombre_grado',$grado->nombre_grado)}}"
                                    required="required"
                        >
                    @if($errors->has('nombre_grado'))
            <p class="text-danger">{{$errors->first('nombre_grado')}}</p>
            @endif
        </div>
                        <div>
            <button class="btn btn-primary" type="submit">Save</button>
            <a href="{{ url()->previous() }}">Back</a>
        </div>
    </form>
    </div>
        </div>

</div>
@endsection