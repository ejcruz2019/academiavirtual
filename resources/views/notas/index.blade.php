@extends('layouts.app')
@section('content')
<div class="container">

    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h1> Notas </h1>
        </div>
    <div class="card-body">

    <div>
        <a href="{{route('notas.create')}}">New</a>
    </div>
    <table class="table table-striped">
        @if(count($notas))
        <thead>
            <tr>
                <th>&nbsp;</th>
                                                <td>Id Nota</td>
                
                                                <td>Inscripcion Id Inscripcion</td>
                
                                                <td>Fecha</td>
                
                                                <td>Nota</td>
                
                            </tr>

        </thead>
        @endif
        <tbody>
            @forelse($notas as $nota)
            <tr>
                <td>
                    <a href="{{route('notas.show',['nota'=>$nota] )}}">Show</a>
                    <a href="{{route('notas.edit',['nota'=>$nota] )}}">Edit</a>
                    <a href="javascript:void(0)" onclick="event.preventDefault();
                    document.getElementById('delete-nota-{{$nota->id}}').submit();">
                        Delete
                    </a>
                    <form id="delete-nota-{{$nota->id}}" action="{{route('notas.destroy',['nota'=>$nota])}}" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                </td>
                                                <td>{{$nota->id_nota}}</td>
                                                                <td>{{$nota->inscripcion_id_inscripcion}}</td>
                                                                <td>{{$nota->fecha}}</td>
                                                                <td>{{$nota->nota}}</td>
                                
            </tr>
            @empty
            <p>No Notas</p>
            @endforelse
        </tbody>
    </table>
    </div>
    </div>

</div>

@endsection