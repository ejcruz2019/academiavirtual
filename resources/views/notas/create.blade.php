@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">

        <div class="card-header">
            <h1> Nota Create </h1>
        </div>
        <div class="card-body">

        @if($errors->any())
        <ul>
            @foreach($errors->all() as $error)
            <li class="text-danger">{{ $error }}</li>
            @endforeach
        </ul @endif <form action="{{route('notas.store')}}" method="POST" novalidate>
        @csrf
        
                        <div class="form-group">
            <label for="id_nota">Id Nota</label>
                        <input class="form-control Integer"  type="number"  name="id_nota" id="id_nota" value="{{old('id_nota')}}"                         required="required"
                        >
                        @if($errors->has('id_nota'))
            <p class="text-danger">{{$errors->first('id_nota')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="inscripcion_id_inscripcion">Inscripcion Id Inscripcion</label>
                        <input class="form-control Integer"  type="number"  name="inscripcion_id_inscripcion" id="inscripcion_id_inscripcion" value="{{old('inscripcion_id_inscripcion')}}"                         required="required"
                        >
                        @if($errors->has('inscripcion_id_inscripcion'))
            <p class="text-danger">{{$errors->first('inscripcion_id_inscripcion')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="fecha">Fecha</label>
                        <input class="form-control Date"  type="date"  name="fecha" id="fecha" value="{{old('fecha')}}"                         required="required"
                        >
                        @if($errors->has('fecha'))
            <p class="text-danger">{{$errors->first('fecha')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="nota">Nota</label>
                        <input class="form-control Integer"  type="number"  name="nota" id="nota" value="{{old('nota')}}"                         required="required"
                        >
                        @if($errors->has('nota'))
            <p class="text-danger">{{$errors->first('nota')}}</p>
            @endif
        </div>
                        <div>
            <button class="btn btn-primary" type="submit">Create</button>
            <a href="{{ url()->previous() }}">Back</a>
        </div>
        </form>
        </div>
    </div>
</div>
@endsection