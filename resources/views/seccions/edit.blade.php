@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card">

        <div class="card-header">
            <h1> Seccion Edit </h1>
        </div>
        <div class="card-body">

    @if($errors->any())
    <ul>
        @foreach($errors->all() as $error)
        <li class="text-danger">{{ $error }}</li>
        @endforeach
    </ul>

    @endif

    <form action="{{route('seccions.update',['seccion'=>$seccion->id])}}" method="POST" novalidate>
        @csrf
        @method('PUT')
        

                        <div class="form-group">
            <label for="id_seccion">Id Seccion</label>
                    <input class="form-control Integer"  type="number"  name="id_seccion" id="id_seccion" value="{{old('id_seccion',$seccion->id_seccion)}}"
                                    required="required"
                        >
                    @if($errors->has('id_seccion'))
            <p class="text-danger">{{$errors->first('id_seccion')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="grado_id_grado">Grado Id Grado</label>
                    <input class="form-control Integer"  type="number"  name="grado_id_grado" id="grado_id_grado" value="{{old('grado_id_grado',$seccion->grado_id_grado)}}"
                                    required="required"
                        >
                    @if($errors->has('grado_id_grado'))
            <p class="text-danger">{{$errors->first('grado_id_grado')}}</p>
            @endif
        </div>
                                <div class="form-group">
            <label for="nombre_seccion">Nombre Seccion</label>
                    <input class="form-control String"  type="text"  name="nombre_seccion" id="nombre_seccion" value="{{old('nombre_seccion',$seccion->nombre_seccion)}}"
                                    >
                    @if($errors->has('nombre_seccion'))
            <p class="text-danger">{{$errors->first('nombre_seccion')}}</p>
            @endif
        </div>
                        <div>
            <button class="btn btn-primary" type="submit">Save</button>
            <a href="{{ url()->previous() }}">Back</a>
        </div>
    </form>
    </div>
        </div>

</div>
@endsection