@extends('layouts.app')
@section('content')
<div class="container">

    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h1> Seccions </h1>
        </div>
    <div class="card-body">

    <div>
        <a href="{{route('seccions.create')}}">New</a>
    </div>
    <table class="table table-striped">
        @if(count($seccions))
        <thead>
            <tr>
                <th>&nbsp;</th>
                                                <td>Id Seccion</td>
                
                                                <td>Grado Id Grado</td>
                
                                                <td>Nombre Seccion</td>
                
                            </tr>

        </thead>
        @endif
        <tbody>
            @forelse($seccions as $seccion)
            <tr>
                <td>
                    <a href="{{route('seccions.show',['seccion'=>$seccion] )}}">Show</a>
                    <a href="{{route('seccions.edit',['seccion'=>$seccion] )}}">Edit</a>
                    <a href="javascript:void(0)" onclick="event.preventDefault();
                    document.getElementById('delete-seccion-{{$seccion->id}}').submit();">
                        Delete
                    </a>
                    <form id="delete-seccion-{{$seccion->id}}" action="{{route('seccions.destroy',['seccion'=>$seccion])}}" method="POST" style="display: none;">
                        @csrf
                        @method('DELETE')
                    </form>
                </td>
                                                <td>{{$seccion->id_seccion}}</td>
                                                                <td>{{$seccion->grado_id_grado}}</td>
                                                                <td>{{$seccion->nombre_seccion}}</td>
                                
            </tr>
            @empty
            <p>No Seccions</p>
            @endforelse
        </tbody>
    </table>
    </div>
    </div>

</div>

@endsection