<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\Models\Contrato\Contrato;
use Faker\Generator as Faker;


$factory->define(Contrato::class, function (Faker $faker) {
    return [
        'id_contrato' => $faker->randomDigit,
        'seccion_id_seccion' => $faker->randomDigit,
        'catedratico_id_catedratico' => $faker->randomDigit,
        'fecha' => $faker->word,
    ];
});
