<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\Models\Grado\Grado;
use Faker\Generator as Faker;


$factory->define(Grado::class, function (Faker $faker) {
    return [
        'id_grado' => $faker->randomDigit,
        'nombre_grado' => $faker->text,
    ];
});
