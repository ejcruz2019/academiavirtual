<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\Models\Catedratico\Catedratico;
use Faker\Generator as Faker;


$factory->define(Catedratico::class, function (Faker $faker) {
    return [
        'id_catedratico' => $faker->randomDigit,
        'nombre' => $faker->text,
        'apellido' => $faker->text,
        'dpi' => $faker->text,
        'telefono' => $faker->text,
        'correo' => $faker->text,
    ];
});
