<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\Models\Alumno\Alumno;
use Faker\Generator as Faker;


$factory->define(Alumno::class, function (Faker $faker) {
    return [
        'id_alumno' => $faker->randomDigit,
        'apellido' => $faker->text,
        'nombre' => $faker->text,
        'dpi' => $faker->text,
        'telefono' => $faker->text,
        'correo' => $faker->text,
    ];
});
