<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\Models\Nota\Nota;
use Faker\Generator as Faker;


$factory->define(Nota::class, function (Faker $faker) {
    return [
        'id_nota' => $faker->randomDigit,
        'inscripcion_id_inscripcion' => $faker->randomDigit,
        'fecha' => $faker->word,
        'nota' => $faker->randomDigit,
    ];
});
