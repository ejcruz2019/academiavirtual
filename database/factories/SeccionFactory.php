<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\Models\Seccion\Seccion;
use Faker\Generator as Faker;


$factory->define(Seccion::class, function (Faker $faker) {
    return [
        'id_seccion' => $faker->randomDigit,
        'grado_id_grado' => $faker->randomDigit,
        'nombre_seccion' => $faker->optional()->text,
    ];
});
