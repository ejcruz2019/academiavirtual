<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\Models\Sucursal\Sucursal;
use Faker\Generator as Faker;


$factory->define(Sucursal::class, function (Faker $faker) {
    return [
        'id_sucursal' => $faker->randomDigit,
        'nombre' => $faker->text,
        'razon_social' => $faker->text,
        'direccion' => $faker->text,
        'telefono' => $faker->text,
        'correo' => $faker->text,
    ];
});
