<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */

use App\Http\Models\Inscripcion\Inscripcion;
use Faker\Generator as Faker;


$factory->define(Inscripcion::class, function (Faker $faker) {
    return [
        'id_inscripcion' => $faker->randomDigit,
        'user_id' => $faker->randomDigit,
        'alumno_id_alumno' => $faker->randomDigit,
        'seccion_id_seccion' => $faker->randomDigit,
        'fecha' => $faker->word,
    ];
});
