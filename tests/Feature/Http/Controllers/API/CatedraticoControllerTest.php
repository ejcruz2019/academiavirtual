<?php

namespace Tests\Feature\Http\API\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Catedratico\Catedratico;
use App\Http\Models\User;

class CatedraticoControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_catedratico_api()
    {
        $catedratico = factory(Catedratico::class)->make();
        $data = $catedratico->attributesToArray();
        $response = $this->json('POST','api/catedraticos',$data);
        $response->assertStatus(201)->assertJson(['created_at'=>true]);
    }

    /**
     * @test
     */
    public function it_updates_catedratico_api()
    {
        $catedratico = factory(Catedratico::class)->create();
        $data = factory(Catedratico::class)->make()->attributesToArray();
        $response = $this->json('PUT','api/catedraticos/'.$catedratico->id,$data);
        $response->assertStatus(200)->assertJson(['updated_at'=>true]);
    }

    /**
     * @test
     */
    public function it_destroys_catedratico_api()
    {
        $catedratico = factory(Catedratico::class)->create();
        $response = $this->json('DELETE','api/catedraticos/'.$catedratico->id);
        $response->assertStatus(200)->assertJson(['deleted_at'=>true]);
        $catedratico->refresh();
        $this->assertDatabaseMissing('catedraticos',['id' => $catedratico->id]);

    }
}
