<?php

namespace Tests\Feature\Http\API\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Inscripcion\Inscripcion;
use App\Http\Models\User;

class InscripcionControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_inscripcion_api()
    {
        $inscripcion = factory(Inscripcion::class)->make();
        $data = $inscripcion->attributesToArray();
        $response = $this->json('POST','api/inscripcions',$data);
        $response->assertStatus(201)->assertJson(['created_at'=>true]);
    }

    /**
     * @test
     */
    public function it_updates_inscripcion_api()
    {
        $inscripcion = factory(Inscripcion::class)->create();
        $data = factory(Inscripcion::class)->make()->attributesToArray();
        $response = $this->json('PUT','api/inscripcions/'.$inscripcion->id,$data);
        $response->assertStatus(200)->assertJson(['updated_at'=>true]);
    }

    /**
     * @test
     */
    public function it_destroys_inscripcion_api()
    {
        $inscripcion = factory(Inscripcion::class)->create();
        $response = $this->json('DELETE','api/inscripcions/'.$inscripcion->id);
        $response->assertStatus(200)->assertJson(['deleted_at'=>true]);
        $inscripcion->refresh();
        $this->assertDatabaseMissing('inscripcions',['id' => $inscripcion->id]);

    }
}
