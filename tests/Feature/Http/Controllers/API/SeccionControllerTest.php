<?php

namespace Tests\Feature\Http\API\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Seccion\Seccion;
use App\Http\Models\User;

class SeccionControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_seccion_api()
    {
        $seccion = factory(Seccion::class)->make();
        $data = $seccion->attributesToArray();
        $response = $this->json('POST','api/seccions',$data);
        $response->assertStatus(201)->assertJson(['created_at'=>true]);
    }

    /**
     * @test
     */
    public function it_updates_seccion_api()
    {
        $seccion = factory(Seccion::class)->create();
        $data = factory(Seccion::class)->make()->attributesToArray();
        $response = $this->json('PUT','api/seccions/'.$seccion->id,$data);
        $response->assertStatus(200)->assertJson(['updated_at'=>true]);
    }

    /**
     * @test
     */
    public function it_destroys_seccion_api()
    {
        $seccion = factory(Seccion::class)->create();
        $response = $this->json('DELETE','api/seccions/'.$seccion->id);
        $response->assertStatus(200)->assertJson(['deleted_at'=>true]);
        $seccion->refresh();
        $this->assertDatabaseMissing('seccions',['id' => $seccion->id]);

    }
}
