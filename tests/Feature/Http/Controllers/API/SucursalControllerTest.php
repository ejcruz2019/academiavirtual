<?php

namespace Tests\Feature\Http\API\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Sucursal\Sucursal;
use App\Http\Models\User;

class SucursalControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_sucursal_api()
    {
        $sucursal = factory(Sucursal::class)->make();
        $data = $sucursal->attributesToArray();
        $response = $this->json('POST','api/sucursals',$data);
        $response->assertStatus(201)->assertJson(['created_at'=>true]);
    }

    /**
     * @test
     */
    public function it_updates_sucursal_api()
    {
        $sucursal = factory(Sucursal::class)->create();
        $data = factory(Sucursal::class)->make()->attributesToArray();
        $response = $this->json('PUT','api/sucursals/'.$sucursal->id,$data);
        $response->assertStatus(200)->assertJson(['updated_at'=>true]);
    }

    /**
     * @test
     */
    public function it_destroys_sucursal_api()
    {
        $sucursal = factory(Sucursal::class)->create();
        $response = $this->json('DELETE','api/sucursals/'.$sucursal->id);
        $response->assertStatus(200)->assertJson(['deleted_at'=>true]);
        $sucursal->refresh();
        $this->assertDatabaseMissing('sucursals',['id' => $sucursal->id]);

    }
}
