<?php

namespace Tests\Feature\Http\API\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Contrato\Contrato;
use App\Http\Models\User;

class ContratoControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_contrato_api()
    {
        $contrato = factory(Contrato::class)->make();
        $data = $contrato->attributesToArray();
        $response = $this->json('POST','api/contratos',$data);
        $response->assertStatus(201)->assertJson(['created_at'=>true]);
    }

    /**
     * @test
     */
    public function it_updates_contrato_api()
    {
        $contrato = factory(Contrato::class)->create();
        $data = factory(Contrato::class)->make()->attributesToArray();
        $response = $this->json('PUT','api/contratos/'.$contrato->id,$data);
        $response->assertStatus(200)->assertJson(['updated_at'=>true]);
    }

    /**
     * @test
     */
    public function it_destroys_contrato_api()
    {
        $contrato = factory(Contrato::class)->create();
        $response = $this->json('DELETE','api/contratos/'.$contrato->id);
        $response->assertStatus(200)->assertJson(['deleted_at'=>true]);
        $contrato->refresh();
        $this->assertDatabaseMissing('contratos',['id' => $contrato->id]);

    }
}
