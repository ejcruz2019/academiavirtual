<?php

namespace Tests\Feature\Http\API\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Grado\Grado;
use App\Http\Models\User;

class GradoControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_grado_api()
    {
        $grado = factory(Grado::class)->make();
        $data = $grado->attributesToArray();
        $response = $this->json('POST','api/grados',$data);
        $response->assertStatus(201)->assertJson(['created_at'=>true]);
    }

    /**
     * @test
     */
    public function it_updates_grado_api()
    {
        $grado = factory(Grado::class)->create();
        $data = factory(Grado::class)->make()->attributesToArray();
        $response = $this->json('PUT','api/grados/'.$grado->id,$data);
        $response->assertStatus(200)->assertJson(['updated_at'=>true]);
    }

    /**
     * @test
     */
    public function it_destroys_grado_api()
    {
        $grado = factory(Grado::class)->create();
        $response = $this->json('DELETE','api/grados/'.$grado->id);
        $response->assertStatus(200)->assertJson(['deleted_at'=>true]);
        $grado->refresh();
        $this->assertDatabaseMissing('grados',['id' => $grado->id]);

    }
}
