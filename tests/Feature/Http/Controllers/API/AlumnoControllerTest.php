<?php

namespace Tests\Feature\Http\API\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Alumno\Alumno;
use App\Http\Models\User;

class AlumnoControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_alumno_api()
    {
        $alumno = factory(Alumno::class)->make();
        $data = $alumno->attributesToArray();
        $response = $this->json('POST','api/alumnos',$data);
        $response->assertStatus(201)->assertJson(['created_at'=>true]);
    }

    /**
     * @test
     */
    public function it_updates_alumno_api()
    {
        $alumno = factory(Alumno::class)->create();
        $data = factory(Alumno::class)->make()->attributesToArray();
        $response = $this->json('PUT','api/alumnos/'.$alumno->id,$data);
        $response->assertStatus(200)->assertJson(['updated_at'=>true]);
    }

    /**
     * @test
     */
    public function it_destroys_alumno_api()
    {
        $alumno = factory(Alumno::class)->create();
        $response = $this->json('DELETE','api/alumnos/'.$alumno->id);
        $response->assertStatus(200)->assertJson(['deleted_at'=>true]);
        $alumno->refresh();
        $this->assertDatabaseMissing('alumnos',['id' => $alumno->id]);

    }
}
