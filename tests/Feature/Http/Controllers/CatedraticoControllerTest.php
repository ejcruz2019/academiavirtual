<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Catedratico\Catedratico;

class CatedraticoControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_catedratico_and_redirects()
    {

        $catedratico = factory(Catedratico::class)->make();
        $data = $catedratico->attributesToArray();
        $response = $this->post(route('catedraticos.store'), $data);
        $response->assertRedirect(route('catedraticos.index'));
        $response->assertSessionHas('status', 'Catedratico created!');
    }

    /**
     * @test
     */
    public function it_updates_catedratico_and_redirects()
    {
        $catedratico = factory(Catedratico::class)->create();
        $data = factory(Catedratico::class)->make()->attributesToArray();
        $response = $this->put(route('catedraticos.update', ['catedratico' => $catedratico]), $data);
        $response->assertRedirect(route('catedraticos.index'));
        $response->assertSessionHas('status', 'Catedratico updated!');
    }

    /**
     * @test
     */
    public function it_destroys_catedratico_and_redirects()
    {
        $catedratico = factory(Catedratico::class)->create();
        $response = $this->delete(route('catedraticos.destroy', ['catedratico' => $catedratico]));
        $response->assertRedirect(route('catedraticos.index'));
        $response->assertSessionHas('status', 'Catedratico destroyed!');
    }
}
