<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Inscripcion\Inscripcion;

class InscripcionControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_inscripcion_and_redirects()
    {

        $inscripcion = factory(Inscripcion::class)->make();
        $data = $inscripcion->attributesToArray();
        $response = $this->post(route('inscripcions.store'), $data);
        $response->assertRedirect(route('inscripcions.index'));
        $response->assertSessionHas('status', 'Inscripcion created!');
    }

    /**
     * @test
     */
    public function it_updates_inscripcion_and_redirects()
    {
        $inscripcion = factory(Inscripcion::class)->create();
        $data = factory(Inscripcion::class)->make()->attributesToArray();
        $response = $this->put(route('inscripcions.update', ['inscripcion' => $inscripcion]), $data);
        $response->assertRedirect(route('inscripcions.index'));
        $response->assertSessionHas('status', 'Inscripcion updated!');
    }

    /**
     * @test
     */
    public function it_destroys_inscripcion_and_redirects()
    {
        $inscripcion = factory(Inscripcion::class)->create();
        $response = $this->delete(route('inscripcions.destroy', ['inscripcion' => $inscripcion]));
        $response->assertRedirect(route('inscripcions.index'));
        $response->assertSessionHas('status', 'Inscripcion destroyed!');
    }
}
