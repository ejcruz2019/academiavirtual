<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Contrato\Contrato;

class ContratoControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_contrato_and_redirects()
    {

        $contrato = factory(Contrato::class)->make();
        $data = $contrato->attributesToArray();
        $response = $this->post(route('contratos.store'), $data);
        $response->assertRedirect(route('contratos.index'));
        $response->assertSessionHas('status', 'Contrato created!');
    }

    /**
     * @test
     */
    public function it_updates_contrato_and_redirects()
    {
        $contrato = factory(Contrato::class)->create();
        $data = factory(Contrato::class)->make()->attributesToArray();
        $response = $this->put(route('contratos.update', ['contrato' => $contrato]), $data);
        $response->assertRedirect(route('contratos.index'));
        $response->assertSessionHas('status', 'Contrato updated!');
    }

    /**
     * @test
     */
    public function it_destroys_contrato_and_redirects()
    {
        $contrato = factory(Contrato::class)->create();
        $response = $this->delete(route('contratos.destroy', ['contrato' => $contrato]));
        $response->assertRedirect(route('contratos.index'));
        $response->assertSessionHas('status', 'Contrato destroyed!');
    }
}
