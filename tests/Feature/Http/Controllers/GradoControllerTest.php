<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Grado\Grado;

class GradoControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_grado_and_redirects()
    {

        $grado = factory(Grado::class)->make();
        $data = $grado->attributesToArray();
        $response = $this->post(route('grados.store'), $data);
        $response->assertRedirect(route('grados.index'));
        $response->assertSessionHas('status', 'Grado created!');
    }

    /**
     * @test
     */
    public function it_updates_grado_and_redirects()
    {
        $grado = factory(Grado::class)->create();
        $data = factory(Grado::class)->make()->attributesToArray();
        $response = $this->put(route('grados.update', ['grado' => $grado]), $data);
        $response->assertRedirect(route('grados.index'));
        $response->assertSessionHas('status', 'Grado updated!');
    }

    /**
     * @test
     */
    public function it_destroys_grado_and_redirects()
    {
        $grado = factory(Grado::class)->create();
        $response = $this->delete(route('grados.destroy', ['grado' => $grado]));
        $response->assertRedirect(route('grados.index'));
        $response->assertSessionHas('status', 'Grado destroyed!');
    }
}
