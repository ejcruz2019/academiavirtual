<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Sucursal\Sucursal;

class SucursalControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_sucursal_and_redirects()
    {

        $sucursal = factory(Sucursal::class)->make();
        $data = $sucursal->attributesToArray();
        $response = $this->post(route('sucursals.store'), $data);
        $response->assertRedirect(route('sucursals.index'));
        $response->assertSessionHas('status', 'Sucursal created!');
    }

    /**
     * @test
     */
    public function it_updates_sucursal_and_redirects()
    {
        $sucursal = factory(Sucursal::class)->create();
        $data = factory(Sucursal::class)->make()->attributesToArray();
        $response = $this->put(route('sucursals.update', ['sucursal' => $sucursal]), $data);
        $response->assertRedirect(route('sucursals.index'));
        $response->assertSessionHas('status', 'Sucursal updated!');
    }

    /**
     * @test
     */
    public function it_destroys_sucursal_and_redirects()
    {
        $sucursal = factory(Sucursal::class)->create();
        $response = $this->delete(route('sucursals.destroy', ['sucursal' => $sucursal]));
        $response->assertRedirect(route('sucursals.index'));
        $response->assertSessionHas('status', 'Sucursal destroyed!');
    }
}
