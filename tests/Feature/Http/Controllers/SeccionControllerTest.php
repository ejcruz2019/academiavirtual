<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Http\Models\Seccion\Seccion;

class SeccionControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_stores_seccion_and_redirects()
    {

        $seccion = factory(Seccion::class)->make();
        $data = $seccion->attributesToArray();
        $response = $this->post(route('seccions.store'), $data);
        $response->assertRedirect(route('seccions.index'));
        $response->assertSessionHas('status', 'Seccion created!');
    }

    /**
     * @test
     */
    public function it_updates_seccion_and_redirects()
    {
        $seccion = factory(Seccion::class)->create();
        $data = factory(Seccion::class)->make()->attributesToArray();
        $response = $this->put(route('seccions.update', ['seccion' => $seccion]), $data);
        $response->assertRedirect(route('seccions.index'));
        $response->assertSessionHas('status', 'Seccion updated!');
    }

    /**
     * @test
     */
    public function it_destroys_seccion_and_redirects()
    {
        $seccion = factory(Seccion::class)->create();
        $response = $this->delete(route('seccions.destroy', ['seccion' => $seccion]));
        $response->assertRedirect(route('seccions.index'));
        $response->assertSessionHas('status', 'Seccion destroyed!');
    }
}
